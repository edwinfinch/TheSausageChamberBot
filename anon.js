/*
anon.js

The Sausage Chamber Bot / Bratwurst
@TheSausageChamberBot

Created by Edwin Finch
December 27th, 2017
*/

/*

=====================
AnonMessage Structure
=====================
{
    type: AnonMessageType,
    text: "If text-based, there will be text here, otherwise this will be undefined.",
    file: { //If photo, video or file based, this object will exist.
        filePath: "filePath/file.png",
        caption: "A caption, if there was one provided, otherwise this will be undefined."
    },
    dispatch: {
        source_message_id: 0, //The message ID for the source message from the user.
        id: "IDIDI", //A 5 character unique ID representing this message.
        ready: true, //Whether or not this message is ready to be sent.
        timestamp: 0 //The Epoch time of when to dispatch this message.
    }
}

 */

var AnonMessageType = {
    Text: "text",
    Photo: "photo",
    Video: "video"
};

var VIDEO_BYTE_SIZE_LIMIT = 20000000;
var QUEUE_FILEPATH = "anon_file_queue";

var bot = undefined;
var BotInfo = undefined;
var endCommandSessionCallback = undefined;
var startCommandSessionCallback = undefined;

var filesystem = require("fs");
var mongojs = require("mongojs");
var database = mongojs("secret database login", [ "bratwurstQueue" ]);

function setBotAndInfo(newBot, newBotInfo){
    bot = newBot;
    BotInfo = newBotInfo;
}

function generateID() {
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    for (var i = 0; i < 5; i++){
        text += possible.charAt(Math.floor(Math.random() * possible.length));
    }

    return text;
}

function addAnonMessageToQueue(anonMessage, callback){
    var randomAddonTimeInMinutes = Math.floor((Math.random() * (60 * 12)) + 10);

    // randomAddonTimeInMinutes = 20;

    anonMessage.dispatch.id = generateID();
    anonMessage.dispatch.ready = true;
    anonMessage.dispatch.timestamp = Date.now() + (randomAddonTimeInMinutes * 60 * 1000);

    database.bratwurstQueue.insert(anonMessage, function(error, result){
        callback(undefined, anonMessage);
    });
}

function anonMessageForDispatchID(dispatchID, callback){
    database.bratwurstQueue.find({
        "dispatch.id": dispatchID
    }, function(error, results){
        if(error){
            callback(error);
            return;
        }

        if(results.length !== 1){
            callback("This message doesn't exist, sorry. Has it already been sent?");
            return;
        }

        callback(error, results[0]);
    });
}

function updateAnonMessageInQueue(anonMessage, callback){
    database.bratwurstQueue.update({
        "dispatch.id": anonMessage.dispatch.id
    }, anonMessage, function(error, results){
        callback(error);
    });
}

function checkAnonMessageQueue(){
    database.bratwurstQueue.find({}, function(error, results){
        if(error){
            bot.sendMessage(BotInfo.UserID.Edwin, "An error occurred when polling the anon message queue, sorry.\n\n" + error, error);
            return;
        }

        var currentTime = Date.now();

        for(var anonMessageIndex in results){
            var anonMessage = results[anonMessageIndex];
            // console.log(JSON.stringify(anonMessage, undefined, 4));
            if(anonMessage.dispatch.timestamp <= currentTime){
                switch(anonMessage.type){
                    case AnonMessageType.Photo:
                        bot.sendPhoto(BotInfo.UserID.TheSausageChamber, anonMessage.file.filePath, { caption: anonMessage.file.caption });
                        break;
                    case AnonMessageType.Text:
                        bot.sendMessage(BotInfo.UserID.TheSausageChamber, anonMessage.text);
                        break;
                    case AnonMessageType.Video:
                        bot.sendVideo(BotInfo.UserID.TheSausageChamber, anonMessage.file.filePath, { caption: anonMessage.file.caption });
                        break;
                }

                if(!BotInfo.Production){
                    bot.sendMessage(BotInfo.UserID.TheSausageChamber, JSON.stringify(anonMessage, undefined, 4));
                }

                database.bratwurstQueue.remove({
                    "dispatch.id": anonMessage.dispatch.id
                });

                if(anonMessage.type === AnonMessageType.Photo){
                    filesystem.unlink(anonMessage.file.filePath, function(error){
                        if(error){
                            bot.sendMessage(BotInfo.UserID.Edwin, "*An error occurred when deleting a photo, sorry.*\n\n" + error, { parse_mode: "Markdown" });
                        }
                    });
                }
            }
        }
    });
}

function timeInMinutesUntilDispatchForAnonMessage(anonMessage){
    var currentTime = Date.now();
    var anonMessageDispatchTime = anonMessage.dispatch.timestamp;
    var timeDifferenceInMilliseconds = (anonMessageDispatchTime - currentTime);

    return Math.ceil((timeDifferenceInMilliseconds / 1000) / 60);
}

function remainingDispatchTimeStringForAnonMessage(anonMessage){
    var remainingTimeInSeconds = (timeInMinutesUntilDispatchForAnonMessage(anonMessage) * 60);
    var remainingDispatchTimeString = (new Date(anonMessage.dispatch.timestamp)).toLocaleTimeString() + ", which is about ";

    var start = Date.now(),
                diff,
                hours,
                minutes,
                seconds;

    diff = remainingTimeInSeconds - (((Date.now() - start) / 1000) | 0);

    minutes = (diff / 60) | 0;
    seconds = (diff % 60) | 0;
    hours = (minutes / 60) | 0;
    minutes = minutes % 60;

    var hourString = (hours === 1) ? "hour" : "hours";
    var minuteString = (minutes === 1) ? "minute" : "minutes";
    var secondString = (seconds === 1) ? "second" : "seconds";

    if(hours > 0){
        remainingDispatchTimeString += hours + " " + hourString;
        if(((minutes > 0) && (seconds === 0)) || ((minutes === 0) && (seconds > 0))){
            remainingDispatchTimeString += " and ";
        }
        else if(!minutes && !seconds){
            remainingDispatchTimeString += "";
        }
        else{
            remainingDispatchTimeString += ", ";
        }
    }
    if(minutes > 0){
        remainingDispatchTimeString += minutes + " " + minuteString + ((seconds > 0) ? " and " : "");
    }
    if(seconds > 0){
        remainingDispatchTimeString += seconds + " " + secondString;
    }

    if(minutes === 0 && hours === 0 && seconds === 0){
        remainingDispatchTimeString += "0 minutes";
    }

    remainingDispatchTimeString += " from now";

    return remainingDispatchTimeString;
}

function setupAnonMessageAndAlertUser(anonMessage, message){
    addAnonMessageToQueue(anonMessage, function(error, finalAnonMessage){
        endCommandSessionCallback(message.from.id);

        if(error){
            bot.sendMessage(message.chat.id, "*Sorry, an error occurred when trying to queue your message.*\n\n_" + error + "_", {
                parse_mode: "Markdown"
            });
            return;
        }

        var timeUntilDispatchForAnonMessage = timeInMinutesUntilDispatchForAnonMessage(finalAnonMessage);

        console.log(JSON.stringify(finalAnonMessage, undefined, 4));

        var queueSuccessMessageString = "*Your message has been queued successfully.*\n\n";
        queueSuccessMessageString += "It will be sent around " + remainingDispatchTimeStringForAnonMessage(finalAnonMessage) + ".\n\n";
        // queueSuccessMessageString += "*Want to change the time that your message gets sent?*\n\nReply to this message with a new delay in the format XhYm where X is the amount of hours from now and Y is the amount of minutes from now.";

        bot.sendMessage(message.chat.id, queueSuccessMessageString, {
            parse_mode: "Markdown",
            reply_to_message_id: message.message_id,
            reply_markup: {
                inline_keyboard: [
                    [
                        {
                            text: "Change sending time",
                            callback_data: BotInfo.Command.Anon + "$change_" + finalAnonMessage.dispatch.id
                        }
                        // {
                        //     text: "Cancel this message"
                        //     callback_data: "cancel_" + finalAnonMessage.dispatch.id
                        // }
                    ]
                ]
            }
        });
    });
}

function handleAnonSessionMessage(message){
    if(message.photo){
        var highestResolutionPhoto = message.photo[message.photo.length - 1];

        bot.sendMessage(message.chat.id, "Hold on...").then(function(queueMessageSent){
            bot.downloadFile(highestResolutionPhoto.file_id, QUEUE_FILEPATH).then(function(result){
                var filePath = result;

                var anonMessage = {
                    type: AnonMessageType.Photo,
                    file: {
                        filePath: filePath,
                        caption: message.caption
                    },
                    dispatch: {
                        source_message_id: message.message_id,
                        ready: false
                    }
                };

                bot.deleteMessage(message.chat.id, queueMessageSent.message_id);

                setupAnonMessageAndAlertUser(anonMessage, message);
            });
        });
    }
    else if(message.text){
        var anonMessage = {
            type: AnonMessageType.Text,
            text: message.text,
            dispatch: {
                source_message_id: message.message_id,
                ready: false
            }
        };

        setupAnonMessageAndAlertUser(anonMessage, message);
    }
    else if(message.video){
        var video = message.video;

        if(video.file_size > VIDEO_BYTE_SIZE_LIMIT){
            bot.sendMessage(message.chat.id, "Sorry, videos must be 20 MB or less.");
            return;
        }

        bot.sendMessage(message.chat.id, "Hold on...").then(function(queueMessageSent){
            bot.downloadFile(video.file_id, QUEUE_FILEPATH).then(function(result){
                var filePath = result;

                var anonMessage = {
                    type: AnonMessageType.Video,
                    file: {
                        filePath: filePath,
                        caption: message.caption
                    },
                    dispatch: {
                        source_message_id: message.message_id,
                        ready: false
                    }
                };

                bot.deleteMessage(message.chat.id, queueMessageSent.message_id);

                setupAnonMessageAndAlertUser(anonMessage, message);
            });
        });

        console.log(JSON.stringify(queueMessageSent, undefined, 4) + "\n\nMessage above\n\n");
    }
    else{
        bot.sendMessage(message.chat.id, "*Sorry, only text and images can be sent through Bratwurst for the time being.*\n\nPlease try again.", { parse_mode: "Markdown" });
    }
}

function handleAnonCallbackQuery(callbackQuery){
    var message = callbackQuery.message.reply_to_message;

    var fullCommand = callbackQuery.data.split("$")[1];
    var commandParts = fullCommand.split("_");
    var commandName = commandParts[0];
    var dispatchID = commandParts[1];

    console.log("Callback message " + JSON.stringify(message, undefined, 4));

    anonMessageForDispatchID(dispatchID, function(error, anonMessage){
        if(error){
            bot.sendMessage(message.chat.id, "*Sorry, an error occurred.*\n\n_" + error + "_", { parse_mode: "Markdown" });
            return;
        }

        switch(commandName){
            case "change":
                startCommandSessionCallback(BotInfo.Command.AnonDelayChange, message.from.id, {
                    dispatchID: dispatchID
                });

                bot.sendMessage(message.chat.id, '*Please send the new delay (in terms of minutes and hours) or an exact time for this message to be sent.*\n\nA few examples: "1 hour 2 minutes" or "45 minutes" or "3:00 PM".\n\n_The delay you send will start at the exact moment you send the message. Feel free to_ /cancel _if you didn\'t mean to do this._',
                {
                    parse_mode: "Markdown",
                    reply_to_message_id: anonMessage.dispatch.source_message_id
                });
                break;
        }
    });
}

function handleAnonDelayChangeMessage(message, commandSession){
    var isExactTime = (message.text.indexOf(":") > -1);

    var exactDate = new Date();
    var currentDate = new Date();

    if(isExactTime){
        var time = message.text.match(/(\d+)(?::(\d\d))?\s*(p?)/);

        exactDate.setHours(parseInt(time[1]) + (time[3] ? 12 : 0));
        exactDate.setMinutes(parseInt(time[2]) || 0);
        exactDate.setSeconds(0);

        if(exactDate.getTime() < currentDate.getTime()){
            exactDate = new Date(exactDate.getTime() + (24 * 60 * 60 * 1000));
        }
    }
    else{
        var pieces = message.text.split(" ");

        var parseError = undefined;
        var minutes = 0;
        var hours = 0;

        if((pieces.length % 2) !== 0){
            parseError = "Invalid format, sorry.";
        }

        try {
            for(var i = 0; i < pieces.length; i += 2){
                var number = parseInt(pieces[i]);
                var unit = pieces[i + 1];
                switch(unit.substring(0, 1).toLowerCase()){
                    case "m":
                        minutes = number;
                        break;
                    case "h":
                        hours = number;
                        break;
                    default:
                        parseError = "Error: I'm not sure what time unit you're using, but it's not supported, sorry.";
                        break;
                }
            }
        }
        catch(error){
            parseError = "An unknown error occurred when parsing your new delay, sorry.";
        }

        if(minutes < 0 || minutes > 59){
            parseError = "Minutes must be between 0 and 59, sorry. Use hours as well if you need something more than 59 minutes.";
        }
        else if(hours < 0 || hours > 23){
            parseError = "Hours must be between 0 and 23, sorry.";
        }
        else if(isNaN(minutes) || isNaN(hours)){
            parseError = "There may have been a typo in one of your numbers, because I could not parse it, sorry.";
        }
    }

    if(parseError){
        var errorMessageText = "*" + parseError + "*\n\n";

        errorMessageText += "Please format the delay like this:\n\n*X hours Y minutes*\n\n";
        errorMessageText += "You can also send an exact time, ie. 2:00 PM\n\n";
        errorMessageText += "Feel free to /cancel if you don't wanna do this anymore.";

        bot.sendMessage(message.chat.id, errorMessageText, { parse_mode: "Markdown" });

        return;
    }

    anonMessageForDispatchID(commandSession.data.dispatchID, function(error, anonMessage){
        if(error){
            bot.sendMessage(message.chat.id, "*Sorry, an error occurred.*\n\n_" + error + "_", { parse_mode: "Markdown" });
            return;
        }

        var newDispatchTime = Date.now();
        if(isExactTime){
            newDispatchTime = exactDate.getTime();
        }
        else{
            newDispatchTime += hours * 60 * 60 * 1000;
            newDispatchTime += minutes * 60 * 1000;
        }

        anonMessage.dispatch.timestamp = newDispatchTime;

        updateAnonMessageInQueue(anonMessage, function(updateError){
            if(updateError){
                bot.sendMessage(message.chat.id, "*Sorry, an error occurred when updating your dispatch time.*\n\n_" + updateError + "_", {
                    parse_mode: "Markdown"
                });
                return;
            }

            var remainingDispatchTimeString = remainingDispatchTimeStringForAnonMessage(anonMessage);

            endCommandSessionCallback(message.from.id);

            bot.sendMessage(message.chat.id, "*Updated delay successfully.*\n\nThis message will now be sent around " + remainingDispatchTimeString + ".", {
                parse_mode: "Markdown",
                reply_to_message_id: anonMessage.dispatch.source_message_id
            });
        });
    });
}

function setCommandSessionCallbacks(newEndCommandSessionCallback, newStartCommandSessionCallback){
    endCommandSessionCallback = newEndCommandSessionCallback;
    startCommandSessionCallback = newStartCommandSessionCallback;
}

setInterval(checkAnonMessageQueue, 10 * 1000);
checkAnonMessageQueue();

module.exports = {
    setBotAndInfo: setBotAndInfo,
    setCommandSessionCallbacks: setCommandSessionCallbacks,
    handleAnonSessionMessage: handleAnonSessionMessage,
    handleAnonCallbackQuery: handleAnonCallbackQuery,
    handleAnonDelayChangeMessage: handleAnonDelayChangeMessage
};