/*
bot.js

The Sausage Chamber Bot / Bratwurst
@TheSausageChamberBot

Created by Edwin Finch
December 26th, 2017
*/

var BotInfo = {
    Production: true,
    Token: "secret bot token",
    Username: "TheSausageChamberBot",
    UserID: {
        Edwin: 1494633,
        TheSausageChamber: 0
    },
    Command: {
        Anon: "anon",
        AnonDelayChange: "changeanondelay"
    }
};

var TelegramBot = require("node-telegram-bot-api");
var Anon = require("./anon");

var token = "[no token, somehow]";
if (BotInfo.Production) {
    console.log("\n\nYou are running a PRODUCTION Bratwurst!\n");

    BotInfo.UserID.TheSausageChamber = -1001154114871;
}
else {
    console.log("\n\nYou are running a SANDBOX Bratwust!\n");

    BotInfo.UserID.TheSausageChamber = -1001340728077;
}

console.log(JSON.stringify(BotInfo, undefined, 4));

var bot = new TelegramBot(BotInfo.Token, {
    polling: true
});

var commandSessions = [ ];

function isPrivate(message){
    return (message.chat.type === "private");
}

function commandSessionInProgress(uuid){
    for(var sessionIndex in commandSessions){
        var session = commandSessions[sessionIndex];

        console.log(JSON.stringify(session, undefined, 4));

        if(session.info.uuid === uuid){
            return session;
        }
    }

    return undefined;
}

function startCommandSession(command, uuid, data){
    // bot.sendMessage(BotInfo.UserID.Edwin, "Session created for command " + command);

    var sessionInProgress = commandSessionInProgress(uuid);
    if(sessionInProgress){
        endCommandSession(sessionInProgress.info.uuid);
    }

    commandSessions.push({
        info: {
            uuid: uuid,
            command: command
        },
        data: data
    });
}

function endCommandSession(uuid){
    for(var sessionIndex in commandSessions){
        var session = commandSessions[sessionIndex];

        // console.log(JSON.stringify(session, undefined, 4));

        if(session.info.uuid === uuid){
            // bot.sendMessage(BotInfo.UserID.Edwin, "Session ENDED for command " + session.info.command);

            commandSessions.splice(sessionIndex, 1);
            return session;
        }
    }

    return undefined;
}

function anonEndCommandSessionCallback(sessionUUID){
    endCommandSession(sessionUUID);
}

function anonStartCommandSessionCallback(command, uuid, data){
    startCommandSession(command, uuid, data);
}

bot.onText(/\/start/, function(message, match){
    if(!isPrivate(message)){
        return;
    }

    bot.sendMessage(message.chat.id, "*Looking to send some shit to* @TheSausageChamber *but don't want people to be suspicious of your last seen time?*\n\nBratwurst is the bot for you.\n\nSend /anon to begin.", {
        parse_mode: "Markdown"
    });
});

bot.onText(/\/anon/, function(message, match){
    if(!isPrivate(message)){
        return;
    }

    var anonSession = {
        dispatch: {
            ready: false,
            time: 0
        }
    };

    startCommandSession(BotInfo.Command.Anon, message.from.id, anonSession);

    bot.sendMessage(message.chat.id, "*Send me some text, a link, a video, or a photo.*\n\n_You can also_ /cancel _if you didn't mean do to this._", { parse_mode: "Markdown" });
});

bot.onText(/\/changeanondelay/, function(message, match){
    if(!isPrivate(message)){
        return;
    }

    bot.sendMessage(message.chat.id, "Please tap the 'Change sending time' button of any message you have queued to change an anonymous message's sending time.");
});

bot.onText(/\/cancel/, function(message, match){
    if(!isPrivate(message)){
        return;
    }

    var sessionCommandEnded = endCommandSession(message.from.id);

    bot.sendMessage(message.chat.id,
        sessionCommandEnded ?
            ("I've cancelled /" + sessionCommandEnded.info.command + " for you.")
            : "There's no need to cancel anything, you don't have any command in progress.",
        {
            parse_mode: "Markdown"
        }
    );
});

bot.on("message", function(message){
    // console.log("Message: " + JSON.stringify(message, undefined, 4));

    if(message.text){
        if(message.text.indexOf("/cancel") > -1){
            return;
        }
        else if(message.text.indexOf("/anon") > -1){
            return;
        }
    }

    // Anon.handleAnonDelayChangeMessage(message, undefined);
    // return;

    var commandSession = commandSessionInProgress(message.from.id);

    if(isPrivate(message) && commandSession){
        switch(commandSession.info.command){
            case BotInfo.Command.Anon:
                Anon.handleAnonSessionMessage(message);
                break;
            case BotInfo.Command.AnonDelayChange:
                Anon.handleAnonDelayChangeMessage(message, commandSession);
                break;
        }
    }

    // if(isPrivate(message) && commandSession){
    //     bot.sendMessage(message.chat.id, "Command session in progress: " + JSON.stringify(commandSession, undefined, 4));
    // }
    // else{
    //     bot.sendMessage(message.chat.id, "No command session in progress, sorry");
    // }
});

bot.on("callback_query", function onCallbackQuery(callbackQuery) {
    var commandSender = callbackQuery.data.split("$")[0];
    switch(commandSender){
        case BotInfo.Command.Anon:
            Anon.handleAnonCallbackQuery(callbackQuery);
            break;
    }
});

bot.on("polling_error", function(error){
    console.log(error);

    bot.sendMessage(BotInfo.EdwinUserID, "*An error occurred, sorry.*\n\n" + error, { parse_mode: "Markdown" });
});


Anon.setBotAndInfo(bot, BotInfo);
Anon.setCommandSessionCallbacks(anonEndCommandSessionCallback, anonStartCommandSessionCallback);


console.log("\nBratwurst booted successfully.\n");